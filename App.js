import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import LoginForm from "./modules/login"

export default function App() {
  return (
    <View style={styles.container}>
      <LoginForm />

      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: '#67be4b',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
