import React, { useCallback, useMemo, useState } from 'react'
import { useForm } from "react-hook-form"
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";

const login = () => new Promise((res, ref) => setTimeout(() => res(), 2000))

export const email = 'email'
export const password = 'password'

const schema = yup.object().shape({
  [email]: yup.string()
  .email()
  .required(),
  [password]: yup
  .string()
  .min(6)
  .matches(/(?=.*[A-Z])/, 'Password must contain at least 1 uppercase alphabetical character')
  .required(),
})

const defaultValues = {
  [email]: '',
  [password]: '',
}

const LoginContainer = ({ children }) => {
  const [isLoading, setLoading] = useState(false)
  const [title, setTitle] = useState('Login Form')
  const { control, handleSubmit, errors, reset } = useForm(
    {
      defaultValues,
      resolver: yupResolver(schema)
    }
  );
  const onSubmit = useCallback(async (data, e) => {
    try {
      setLoading(true)
      await login(data)
      reset();
      setTitle('Logined')

      setTimeout(
        () => setTitle('Login Form'),
        2000
      )
    } catch (e) {
      console.dir(e)
    } finally {
      setLoading(false)
    }
  }, []);

  const props = useMemo(
    () => ({
      onSubmit: handleSubmit(onSubmit),
      control,
      errors,
      isLoading,
      title
    }),
    [isLoading, errors, control, handleSubmit, onSubmit, title]
  )
  return children(props)
}

export default LoginContainer
