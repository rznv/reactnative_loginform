import React from 'react'
import LoginContainer from "./LoginContainer.js"
import LoginView from "./LoginView.js"

const LoginForm = () => {
  return (
    <LoginContainer>{LoginView}</LoginContainer>
  )
}

export default LoginForm
