import React from 'react'
import { StyleSheet, View } from "react-native"
import Input from "../../components/Input"
import Button from "../../components/Button"
import { Text } from "react-native"
import { Controller } from "react-hook-form";
import { email, password } from "./LoginContainer.js"

const LoginView = (
  {
    control,
    isLoading,
    onSubmit,
    errors,
    title
  }) => (
  <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>

      <Controller
        control={control}
        render={
          ({ onChange, onBlur, value }) => (
            <Input
              placeholder="Enter email"
              error={errors[email]}
              onBlur={onBlur}
              onChange={onChange}
              value={value}
              autoCompleteType="email"
              textContentType="emailAddress"
            />
          )
        }
        defaultValue=""
        name={email}
      />
      <Controller
        control={control}
        render={
          ({ onChange, onBlur, value }) => (
            <Input
              placeholder="Enter password"
              error={errors[password]}
              onBlur={onBlur}
              onChange={onChange}
              value={value}
              autoCompleteType="password"
              textContentType="password"
              secureTextEntry
            />
          )
        }
        defaultValue=""
        name={password}
      />
      <Button
        title={isLoading ? "Loading..." : "Submit"}
        onPress={onSubmit}
        disabled={isLoading}
      />

  </View>
)

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    maxWidth: 300,
    width: '100%',
    paddingTop: 30,
    paddingRight: 20,
    paddingBottom: 30,
    paddingLeft: 20,
    borderRadius: 8,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
    backgroundColor: '#fff'
  },
  title: {
    fontSize: 16,
    color: 'black',
    textAlign: 'center',
    marginBottom: 30,
    fontWeight: "600"
  }
});

export default LoginView
