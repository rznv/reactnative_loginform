import React from 'react'
import { StyleSheet } from "react-native"
import { TextInput, Text, View } from "react-native"

const Input = (
  {
    onChange = () => null,
    error,
    value="",
    ...props
  }) => {
  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        onChangeText={onChange}
        value={value}
        {...props}
      />
      {error && <Text style={styles.error}>{error.message}</Text>}
    </View>
  )
}

const styles = StyleSheet.create({
  input: {
    height: 40,
    width: '100%',
    padding: 10,
    alignSelf: 'stretch',
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 8,
  },
  error: {
    marginTop: 5,
    marginLeft: 5,
    color: 'red',
    fontSize: 12
  },
  container: {
    position: 'relative',
    marginBottom: 10,
  }
});

export default Input
