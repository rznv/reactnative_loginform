import React from 'react'
import { Button as NativeButton } from "react-native"

const Button = (props) => {
  return (
    <NativeButton {...props}/>
  )
}

export default Button
